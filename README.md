# Java Libs

Collection of libs used in the Java projects from the group. You can pull this on itself or add it as a submodule to other projects, hence saving space on the repository.

## Includes

This includes the following Apache common libs:

* Apache Commons Codec 1.10
* Apache Commons Lang 3.4

For using Apache Kafka messaging, these libs are included:

* SL4J 1.7.21
* Kafka 2.11 - 0.10.0
* Kafka Clients - 0.10.0

For the Excel manipulation functionalities, the following libraries are in place:

* DOM4J 1.6.1
* Apache POI 3.10.1
* Apache POI OOXML 3.10.1
* Apache POI ExcelAnt 3.10.1
* Apache POI OOXML Schemas 3.10.1
* Apache XML Beans 2.6.0

For the JMS utilities package, the following libraries are in place:

* jms.jar
* tibjms.jar